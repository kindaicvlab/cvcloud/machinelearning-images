# コントリビュートの手順

1. 変更する必要がある事項に対して template を用いて Issue を発行します．(Issue 上で他の開発者と変更に対して合意が取れているとスムーズに作業が進みます．)
2. 新しいブランチを作成して該当箇所を変更します．ブランチ名は変更に関連する用語が適切です．(ドキュメントを更新する際の例: update-documents)
3. リモートブランチに push します．
4. master ブランチへの Merge Request をテンプレートを用いて作成します．その際，1 で作成した Issue 番号を貼り付けます．
5. 作業中の場合は Draft マークをつけます．
6. 変更が完了して，全てのビルドおよびテストに合格したら iwai 及び M1 1名に レビューリクエストを送ります．
7. 2名から Approval が付いたら Merge します．

# ライブラリバージョンのアップデート基準

本リポジトリが管理するコンテナイメージには以下のような 5 つのステータス及び 1 つの付帯事項を表すステータスが存在します．

|  STATUS  |Descriptioin|
|:--------:|:----------:|
|  closed  |セキュリティパッチや深刻なエラー修正も提供されないので，直ちに使用を停止してください．|
|deprecated|セキュリティパッチや深刻なエラー修正のみが提供されます．各ライブラリが最新版に更新されることはありません．|
|  stable  |安定版です．全てのライブラリが最新版に保たれています．|
|  latest  |最新安定版です．全てのライブラリが最新版に保たれており，サーバでの動作は確認していますが，一部動作しない環境があります．|
|  feature |新機能追加中のため特定の環境でしか動作しません．|
|   alpha  |一部 alpha バージョンのパッケージがインストールされていることを示します.|

付帯ステータスである alpha は nightly バージョンのライブラリを使用している場合やサーバで対応していないバージョンの cuda を使用している場合につける必要があります．


それぞれ 5 つのステータスごとにライブラリのアップデート基準が存在するので，以下に記述します．基準について変更する必要があると感じた場合は Issue を発行してください．

- ベースイメージ cuda バージョンの選定基準と 1 週間毎の自動ビルドの有無

|STATUS|cuda|scheduled|
|:----:|:--:|:-------:|
|closed|-|無し|
|deprecated|-|有り|
|stable|サーバで動作する|有り|
|latest|サーバで動作する|有り|
|feature|サーバで動作しなくても良い|有り|


## closed

本ステータスのコンテナイメージは完全に開発を停止していることを表しています．
通常コンテナイメージはセキュリティパッチなどを当てるため 1 週間ごとに自動で再ビルドされていますが，closed ステータスのイメージは行われません．
そのため closed ステータスのコンテナイメージは何も更新してはいけません．

また，closed ステータスは，以下のいずれかの条件を満たした物とします．

- 最新の cudnn の一つ前のバージョンの[サポートリスト](https://docs.nvidia.com/deeplearning/cudnn/support-matrix/index.html)から外れた．
- [base イメージをビルドしているリポジトリ](https://gitlab.com/nvidia/container-images/cuda)から削除された．


## deprecated

本ステータスのコンテナイメージは使用が非推奨であることを示します．そのため，1 週間に 1度のセキュリティアップデートのみ実行し，その他のライブラリアップデートは実行しません．

ベースイメージに stable ステータスの cuda バージョンより 1 世代以上古い cuda を使用するコンテナイメージが deprecated ステータスとなります．

## stable

本ステータスのコンテナイメージは安定板であることを示します． そのため，1 週間に 1 度のセキュリティアップデートの実行およびすべてのライブラリを nightly バージョンを除く最新版に　1 ヶ月に 1回更新する必要があります.

## latest

本ステータスはコンテナイメージが最新安定板であることを示します．本ステータスは stable 版のライブラリに一部動作しない特異環境が存在する場合，それを nightly バージョンのライブラリを用いて改善した物になります．そのため，stable において特異環境が存在しない場合は latest バージョンを作成する必要がありません．

例えば現在，Pytorch の最新版 1.7.1 において Ampere 世代 GPU が動作しない問題があります．これを改善するため torch-nightly 1.8.0 を使用した コンテナイメージを作成し，latest バージョンとして管理しています．

## feature

本ステータスはコンテナイメージが機能追加中であることを示します．本ステータスはサーバで動作しない場合であっても，全てを nightly も含む最新版に更新します．

例えば現在，サーバで動作する cuda バージョンは 11.0.3 までですが，ベースイメージに cuda 11.1 を使ったコンテナイメージを作成し，feature としています．

# ドキュメントを書く際の参照ドキュメント等

[cuda, cudnn, nvidia driver 対応表](https://docs.nvidia.com/deeplearning/cudnn/support-matrix/index.html)

## インストールされた cudnn のバージョン

- [cuda10.2-cudnn7-devel-ubuntu18.04](https://gitlab.com/nvidia/container-images/cuda/-/blob/master/dist/10.2/ubuntu18.04/devel/cudnn7/Dockerfile#L6)
- [cuda10.2-cudnn8-devel-ubuntu18.04](https://gitlab.com/nvidia/container-images/cuda/-/blob/master/dist/10.2/ubuntu18.04/devel/cudnn8/Dockerfile#L6)
- [cuda11.0.3-cudnn8-devel-ubuntu20.04](https://gitlab.com/nvidia/container-images/cuda/-/blob/master/dist/11.0.3/ubuntu20.04/devel/cudnn8/Dockerfile#L6)
- [cuda11.1.1-cudnn8-devel-ubuntu20.04](https://gitlab.com/nvidia/container-images/cuda/-/blob/master/dist/11.1.1/ubuntu20.04/devel/cudnn8/Dockerfile#L6)
- [cuda11.2.0-cudnn8-devel-ubuntu20.04](https://gitlab.com/nvidia/container-images/cuda/-/blob/master/dist/11.2.0/ubuntu20.04/devel/cudnn8/Dockerfile#L6)
- [cuda11.2.1-cudnn8-devel-ubuntu20.04](https://gitlab.com/nvidia/container-images/cuda/-/blob/master/dist/11.2.1/ubuntu20.04/devel/cudnn8/Dockerfile#L6)
- [cuda11.2.2-cudnn8-devel-ubuntu20.04](https://gitlab.com/nvidia/container-images/cuda/-/blob/master/dist/11.2.2/ubuntu20.04/devel/cudnn8/Dockerfile#L6)
- [cuda11.3.0-cudnn8-devel-ubuntu20.04](https://gitlab.com/nvidia/container-images/cuda/-/blob/master/dist/11.3.0/ubuntu20.04/devel/cudnn8/Dockerfile#L6)
- [cuda11.3.1-cudnn8-devel-ubuntu20.04](https://gitlab.com/nvidia/container-images/cuda/-/blob/master/dist/11.3.1/ubuntu20.04/devel/cudnn8/Dockerfile#L6)
- [cuda11.4.0-cudnn8-devel-ubuntu20.04](https://gitlab.com/nvidia/container-images/cuda/-/blob/master/dist/11.4.0/ubuntu20.04/devel/cudnn8/Dockerfile#L6)
